
<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/API_Controller.php';
class Pokemon_Api extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function simple_api()
    {

        header("Access-Control-Allow-Origin: *");

        // API CONFIG
        $this->_apiConfig(array(
            'methods' => array('POST', 'GET')
        ));

    }

    public function api_limit()
    {
        /**
         * API Limit
         * ----------------------------------
         * @param: {int} API limit Number
         * @param: {string} API limit Type (IP)
         * @param: {int} API limit Time [minute]
         */

        $this->_APIConfig(array(
            // number limit, type limit, time limit (last minute)
            'limit' => array(50, 'ip', 'everyday'),
        ));
    }

    public function api_key(){
        /**
         * Use API Key without Database
         * ---------------------------------------------------------
         * @param: {string} Types
         * @param: {string} API Key
         */
        $this->_APIConfig(array(
            'methods' => array('POST'),
            //'key' => array('header', '13d1266d-81bd-4dc4-920f-eab672a802df'),

            // API Key With Database
            'key' => array('header'),

            // Add Custom data in API Response
            'data' => array(
                'is_login' => false
            ),

        ));

        // Data
        $data = 0;

        /**
         * Return API Response
         * ---------------------------------------------------------
         * @param: API Data
         * @param: Request Status Code
         */
        if (!empty($data)){
            $this->api_return($data, '200');
        }else{
            $this->api_return(array('status' => false, 'error' => 'Invalid Data'), '404');
        }

    }

    public function login(){
        header("Access-Control-Allow-Origin: *");

        // API Configuration
        $this->_apiConfig(array(
            'methods' => array('POST'),
        ));

        // you user authentication code will go here, you can compare the user with the database or whatever
        $payload = array(
            'id' => "Your User's ID",
            'other' => "Some other data",
        );

        // Load Authorization Library or Load in autoload config file
        $this->load->library('authorization_token');

        // generate a token
        $token = $this->authorization_token->generateToken($payload);

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'token' => $token,
                ),

            ),
            200);
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        $user_data = $this->_apiConfig(array(
            'methods' => array('POST'),
            'requireAuthorization' => true,
        ));

        //die(var_dump($user_data));

        // return data
        $this->api_return(
            array(
                'status' => true,
                "result" => array(
                    'user_data' => $user_data['token_data']
                ),
            ),
            200);
    }

    public function pokemons() {

        header('Access-Control-Allow-Origin: *');
        // Configuration des méthodes et autorisation de la connexion avec un token
        $this->_APIConfig(
            array(
                'methods' => array('GET', 'POST', 'DELETE'),
                'requireAuthorization' => true
            )
        );
        // Récupérer le type de requête envoyée
        $requestType = $this->input->server('REQUEST_METHOD');

        // Traitement en fonction du type de requêtes via un switch

        switch ($requestType) {

            // Si la requête http est de type GET
            case 'GET':
                $pokemons = $this->db->select('*') // Je selectionne les champs
                    ->from('pokemons') // Je selectionne la table 'pokemons'
                    ->get() // Je récupère la table et son contenu
                    ->result(); // On exploite les données en mode Objet
                //die(var_dump($pokemons));

            // header('Content-Type:application/json');
            // echo json_encode($pokemons);
            $this->api_return($pokemons, 200);
            break;

            // Si la requête http est de type POST
            case 'POST':
                // Traiter les données
                $data['pok_name'] = $this->input->post('pok_name');
                $data['pok_hp'] = $this->input->post('pok_hp');
                $data['pok_img_url'] = $this->input->post('pok_img_url');
                $data['pok_description'] = $this->input->post('pok_description');
                $data['pok_type_1'] = $this->input->post('pok_type_1');
                $data['pok_type_2'] = $this->input->post('pok_type_2');

                // Si jamais on a un id spécifié alors on modifie
                // Exemple : localhost/api/v1/pokemons/[ID]
                //              0    / 1 / 2/ 3 / 4
                $id = $this->uri->segment(4);

                // Si l'id est spécifié
                if($id) {
                    // Faire une fonction qui retourne l'existance de l'id
                    if ($this->checkIfIdExist($id)) {
                        // Si l'id existe on met à jour le pokémon.
                        $status = 'modifié';
                        $this->db->where('id', $id)
                            ->update('pokemons', $data);
                    }
                } else {
                    // Sinon on crée un nouveau pokemon
                    $status = 'ajouté';
                    // Insertion du pokémon en Base de données
                    $this->db->insert('pokemons', $data);
                }

                //die(var_dump($id));
                // Si une ligne en Base de données a été affectée
                if ($this->db->affected_rows() > 0) {
                    // Dans ce cas là on renvoie un succès
                    $response = array('success' => "Votre pokémon a été $status avec succès");

                } else {
                    // Sinon on renvoie une erreur
                    $response = array('error' => "Un problème est survenu lors de l'ajout du pokémon");
                }

                $this->api_return($response, 200);
                break;

            case 'DELETE':
                // Récupérer l'id via uri->segment
                // Checker si l'id existe avec checkIfIDExist($id)
                // Si l'id existe on supprime le pokémon db->where(id, $id)->delete('pokemons')
                // $response = success
                // Si l'id n'existe pas alors on retourne $response = error

                //die(var_dump($this->input->post()));
            break;

        }

        //die(var_dump($requestType));

        //$this->api_return('ok', 200);
    }

    public function checkIfIDExist($id) {

        $verification = $this->db->select('id') // Selection du paramètre ID
        ->from('pokemons') // Dans la table pokemons
        ->where('id', $id) // Où l'id est égal au paramètre $id
        ->get() // Execution de la requête
        ->row(); // Conversion en Object

        // Mode classique
        if ($verification) {
            return true;
        } else {
            return false;
        }

        // Mode ternaire
        //return $verification ? true : false;
    }
}
